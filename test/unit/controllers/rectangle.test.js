describe("Module Rectangles", () => {
    describe("GET /rectangles/sequence", () => {
        it('should fail', () => {
            return server.run('/rectangles/a,2,b').then(expectBadRequest)
        })

        it('should work', () => {
            return server.run('/rectangles/1,2').then((res) => {
                res.statusCode.should.equal(200);
                res.result.should.equal("<p style=\'font-family: Courier New\'>2&emsp;&nbsp;&emsp;$<br>1&emsp;$&emsp;$<br>+&emsp;1&emsp;2<br><br> Aire : 2</p>");
            })
        })
    })

    describe("GET /rectangles/largeur/hauteur", () => {
        it('should fail', () => {
            return server.run('/rectangles/a/2').then(expectBadRequest)
        })

        it('should work', () => {
            return server.run('/rectangles/1/2').then((res) => {
                res.statusCode.should.equal(200);
            })
        })
    })

    // ---------------------------------------------------------------------------
    // Some methods that will help us with the tests
    // ---------------------------------------------------------------------------
    function expectBadRequest(res) {
        res.statusCode.should.equal(400);
        res.result.error.should.equal("Bad Request");
        expect(res.result.error).to.be.equal("Bad Request");
    }
})

Explication du fonctionnement des Playbooks :

Playbook d'installation : Il installe tout le necessaire pour faire tourner l'appli sur les serveurs de prod et preprod.  
 commande : ansible-playbook install.yml

Playbooks de déploiement : au nombre de 2 (un pour la prod et un pour la preprod) Ils permettent de choisir la version de l'appli que l'on souhaite déployer.  
 commande : ansible-playbbok (pre)prod_deploy.yml --extra-vars "version=toto"  
 toto correspond à un tag ou une branche par exemple.

---------------------------------------------

Procédure d'installation d'ansible-tower :

 wget http://releases.ansible.com/ansible-tower/setup/ansible-tower-setup-latest.tar.gz  
 tar xzvf ansible-tower-setup-latest.tar.gz  
 cd ansible-tower-setup-<version>/  
 sudo vim inventory  
 ANSIBLE_SUDO=True ./setup.sh  
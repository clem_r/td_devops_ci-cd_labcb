# Projet CI/CD/Monitoring
## Projet réalisé par : 
- Nicolas Auger, 
- Clément Romac, 
- Jean-Pierre Bachmann, 
- Rodolphe Bourgouin, 
- Pierre Leroy, 
- Cédric Prezelin

## Répartition des tâches : 
-------------------------

### Clément Romac: 
- Gestion Git / Slack / Bitbucket
- TD Monitoring
- Tickets Dev

### Jean-Pierre Bachmann: 
- Tickets Ops

### Rodolphe Bourgouin: 
- Tickets Dev

### Pierre Leroy:
- Tickets Dev

### Nicolas Auger:
- Tickets Dev

### Cedric Prezelin:
- Tickets Dev


## Mal passé :
-------------------------
- Les tickets dev ont été beaucoup plus long que l'on pensait. N'ayant en plus qu'un seul ops dans notre groupe, on a eu beaucoup de mal à faire avancer la partie Ops.

## Bien passé :
-------------------------
- On a plutôt bien cadré la partie Bitbucket/Git dès le début et donc on n'a pas eu de problème sur cette partie.

## Ce que l'on aurait dû faire  :
-------------------------
- Gérer les tags dès le début.

## Etat du projet:
-----------
### Machines:
- Manager - 34.223.230.240
- Worker 1/preprod - 54.70.135.244
- Worker 2/prod - 52.88.121.133
- La clé est dans le fichier devops_infra.pem

### Fait :
- On a mis en place un trello et un slack avec un bot qui nous dit tous les commits/push.
- Tickets : 27,28,29,30 (mais pas installé sur les machines AWS),51,52,53,60,61,79,150,155,172
 
### A faire 
- Tickets : 35
 
### Ne fonctionne pas :
- Les tests unitaires sur la bdd pour le ticket 53.
 
### TD Monitoring :
#### Fait : 
- Tous les appels sur le projet NodeJS sont ajoutés à statsd dans le router. Le td a été fait sur les machines AWS avec un tableau de bord : 
![Alt text](/monitoring/dashboard.png?raw=true)

#### A faire : 
- Les commandes n'ont pas été intégrées aux playbooks ANSIBLE

#### Ne fonctionne pas : 
- La rétention des données ne fonctionne pas.

-------------------------

### Choix et fonctionnement de l'approche de Déploiement Continu utilisé :

Tout passe par les playbooks ansible.
Un fichier hosts comprenant le manager (ansible) ainsi que les 2 workers (preprod et prod)
Un playbook pour l'installation des serveurs (encore en cours de modification afin d'intégrer la partie monitoring).
Un playbook de déploiement intégrant une variable permmettant l'utilisation de l'argument --extra-vars lors du lancement (déploiement d'une version précise de l'app).

Pour le Déploiement Continu nous avons donc logiquement opté pour l'utilisation d'ansible-tower via bitbucket-pipeline.
Les machines fournies par amazon n'étant pas assez performantes (type d'instance t2.micro trop faible) l'installation de tower n'a pas été finalisée.
Nous pouvons quand même faire des tests sur nos propres machines après avoir fait l'install (la procédure se trouve dans le fichier ANSIBLE.md)

----------------------------------------------------------------------
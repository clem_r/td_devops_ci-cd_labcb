// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');
var mongoose = require('mongoose');
const Operation = require('../models/operation.js');

function precision(a) {
  if (!isFinite(a)) return 0;
  var e = 1, p = 0;
  while (Math.round(a * e) / e !== a) { e *= 10; p++; }
  return p;
}

module.exports.getAdd = function(request, reply){
  // parseFloat convert anything to either a float number or NaN
  term1 = parseFloat(request.params.term1)
  term2 = parseFloat(request.params.term2)

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }

  fixNumber = precision(term1) > precision(term2) ? precision(term1) : precision(term2)

  WriteBdd("+", term1 , term2 );
  reply(parseFloat((term1 + term2).toFixed(fixNumber)))
}

module.exports.getDivide = function(request, reply){
  dividend = parseFloat(request.params.dividend);
  divisor = parseFloat(request.params.divisor);

  if(isNaN(dividend) || isNaN(divisor)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }

  if(divisor === 0) {
    return reply(Boom.badRequest('Divisor can not be 0.'))
  }


WriteBdd("/", dividend , divisor );
  reply(dividend / divisor)
}

module.exports.getSoustraction = function(request, reply){
  // parseFloat convert anything to either a float number or NaN
  term1 = parseFloat(request.params.term1)
  term2 = parseFloat(request.params.term2)

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }

  WriteBdd("-", term1 , term2 );
  reply(term1 - term2)
}

module.exports.getMultiplication = function(request, reply){
  multiplicator = parseFloat(request.params.multiplicator)
  multiplicator2 = parseFloat(request.params.multiplicator2)

  if(isNaN(multiplicator) || isNaN(multiplicator2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }

  if(multiplicator === 0 || multiplicator2 === 0) {
    return reply(Boom.badRequest('multiplicator can not be 0.'))
  }

  WriteBdd("*", multiplicator , multiplicator2 );
  reply(multiplicator * multiplicator2)
}

module.exports.history = function(request, reply){
  Operation.find({}).exec(function(err, result) {
    if (!err) {
      return reply(result);
    }
  })
};

function WriteBdd( operator, operand1, operand2) {
  var operation1 = new Operation({operator, operand1, operand2});
  operation1.save(function (err, userObj) {
    if (err) {
      console.log(err);
    } else {
      console.log('saved successfully:', userObj);
    }
  });
}

// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');

var barSequencestest = [1,2,3,4];

module.exports.getTab = function(request, reply){
    sequence = request.params.sequence.split(",")
    if(sequence
        .filter(function(i){
            return isNaN(i);
        }).length > 0)
    {
        return reply(Boom.badRequest('All sequences must be numbers'))
    }
    reply(Remplir(sequence))
}

function Remplir(barSequences) {

    var stringResult = []
    for (var i = Math.max(...barSequences); i >= 0; i--) {
        stringResult[i] = []
        stringResult[i][0] = (i == 0 ? "+" : i)
        for (var j = 1; j <= barSequences.length; j++) {
            if(i == 0){
                stringResult[i][j] = (j)
            }
            else{
                stringResult[i][j] = (barSequences[j-1] >= i ? "$" : "&nbsp;")
            }
        }
        //stringResult[i] = stringResult[i] + "<br>"
    }

    result = stringResult.reverse()
    area = calculateArea(result, barSequences)
    return "<p style='font-family: Courier New\'>" + result
            .map(e => e.join('&emsp;'))
            .join('<br>')
        + "<br><br> Aire : "+area+"</p>"
}

module.exports.getRandomTab = function(request, reply){
    largeur = request.params.largeur;
    hauteur = request.params.hauteur;
    if(isNaN(largeur) || isNaN(hauteur)) {
      return reply(Boom.badRequest('At least one of the term is not a number.'))
    }
    reply(Fill(largeur, hauteur))
}

function Fill(width, height){
    var points = [];

    for(p=0; p<largeur; p++){
        points[p] = Math.floor((Math.random() * hauteur) + 1);
    }
    return Remplir(points);
}

function getPeaks(tab) {
    return tab.filter(function (item, pos) {
        return tab.indexOf(item) == pos;
    })
}

function calculateArea(rect, Tabl) {
    var Aires = []
    var sommets = getPeaks(Tabl)
    var aireMax = 0

    for (var i = 0; i < sommets.length; i++) {
        Aires.push({valeurSommet: sommets[i], val: 0})
    }

    for (var colIndex in rect[0]) {
        var SommetTmp = Tabl[colIndex]
        for (var i = 0; i < Aires.length; i++) {
            if (Aires[i].valeurSommet > SommetTmp)
                Aires[i].val = 0
            else if (SommetTmp >= Aires[i].valeurSommet)
                Aires[i].val = (parseInt(Aires[i].val) + parseInt(Aires[i].valeurSommet))

            if (aireMax < Aires[i].val)
                aireMax = Aires[i].val
        }
    }
    return aireMax
}

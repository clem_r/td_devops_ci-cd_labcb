// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');

module.exports.index = function(request, reply){
  reply.redirect("/diamond/new")
}

module.exports.showForm = function(request, reply){
  reply("<html>" +
      "<body>" +
      "<form action='/diamond' method='post'>" +
      "<input type='text' name='width'>" +
      "<input type='submit'>" +
      "</form>" +
      "</body>")
}

module.exports.generate = function(request, reply){
  var width = parseInt(request.payload.width)
    if(isNaN(width) || !(width & 1)) {
        return reply(Boom.badRequest('Width is null or not even'))
    }

  reply(generateDiamond(width))
}

function generateDiamond(maxWidth) {
    originalWidth = maxWidth
    maxWidth = Math.abs(maxWidth)
    diamond = []
    maxStars = filledString(maxWidth, '+')
    maxSpaces = filledString(maxWidth, '.')

    for (i = 0; i < maxWidth; i ++){
        spaces = maxSpaces.substring(0, startIndex(maxWidth, i))
        stars = maxStars.substring(0, width(maxWidth, i))
        diamond.push(spaces + stars + spaces)
    }

    if(originalWidth < 0)
      return invert(diamond.join("\n"))
    else
      return diamond.join("\n")
}

function width(max, line) {
    return  -Math.abs(line - (max - 1) / 2) * 2 + max;
}

function  startIndex(max, line) {
    return Math.abs(line - (max + 1) / 2 + 1);
}

function filledString(max, char) {
    result = new Array(5)
    result.fill(char)
    return result.join("")
}

function findAndReplace(string, target, replacement) {

    var i = 0, length = string.length;

    for (i; i < length; i++) {

        string = string.replace(target, replacement);

    }

    return string;

}


function invert(diamond){
    diamond = findAndReplace(diamond, "+", "-")
    diamond = findAndReplace(diamond, ".", "+")
    diamond = findAndReplace(diamond, "-", ".")

    return diamond
}

var mongoose = require('mongoose');

const Boom = require('boom');

var Operation = new mongoose.Schema({
    operator: String,
    operand1: Number,
    operand2: Number
});

module.exports = mongoose.model('Operation', Operation);

/*var operation = mongoose.model('Operation', Operation);

module.exports = {
    insert: (operator, operand1, operand2) => {
        var newLog = new operation({
            operator: operator,
            operand1: operand1,
            operand2: operand2
        })
    },

    insert: (teamName) => {
        var newTeam = new teamModel({
            teamId: require('uuid').v4(),
            teamName: teamName,
            users: [],
            createdAt: Date.now()
        })
        return newTeam.save()
    }
}*/